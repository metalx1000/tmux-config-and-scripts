# tmux-config-and-scripts

Copyright Kris Occhipinti 2024-06-26

(https://filmsbykris.com)

License GPLv3

Install
```bash
#back up current config
[[ -f "$HOME/.tmux" ]] && cp $HOME/.tmux $HOME/.tmux.old

git clone "https://gitlab.com/metalx1000/tmux-config-and-scripts.git" || exit 1
cd tmux-config-and-scripts || exit 1

cp -vr tmux.conf $HOME/.tmux
cp -vr plugins $HOME/.tmux/ 
sudo cp -vr scripts/* /usr/local/bin/ 
```

